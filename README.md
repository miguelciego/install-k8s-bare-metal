# install-k8s-bare-metal
## Instalación de un Cluster Kuberntes sobre CentOS 7.6.1810 Metal Desnudo (bare-metal)

Se instalan 3 servidores (Uno master y 2 servidores para ejecutar las aplicaciones worker) con las aplicaciones básicas para ejecutar el cluster de kubernetes (Tener en cuenta que para habiltar br_netfilter hay que ejecutar ``` echo "br_netfilter" > /etc/modules-load.d/br_netfilter.conf ```) según la documentacion en [www.server-world.info - CentOS_7 - Kubernetes : Kubeadm : Install](https://www.server-world.info/en/note?os=CentOS_7&p=kubernetes&f=3), Si se va a utilizar almacenamiento persistente en servidor nfs, se le debe adicionar soporte para nfs segun la documentacion [www.server-world.info - CentOS_7 - Configure NFS Server](https://www.server-world.info/en/note?os=CentOS_7&p=nfs&f=1), para utilizar el Persistent Storage como en el ejemplo [www.server-world.info - CentOS_7 - Kubernetes : Use Persistent Storage](https://www.server-world.info/en/note?os=CentOS_7&p=kubernetes&f=6)


En el nodo Master se ejecuta el comando ```kubeadm init ``` después se instala un controlador de red como indica al final de la ejecución del comando 
> You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

De los addons listados en [Installing Addons](https://kubernetes.io/docs/concepts/cluster-administration/addons/) nosotros vamos a instalar el 'weave' como indica la documentacion [Integrating Kubernetes via the Addon](https://www.weave.works/docs/net/latest/kubernetes/kube-addon/) con el comando
```kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"```

Se debe tener la siguiente respuesta
```
[root@k8s-ma1 ~]# kubectl get nodes
NAME      STATUS   ROLES    AGE     VERSION
k8s-ma1   Ready    master   6m14s   v1.13.1
[root@k8s-ma1 ~]#
```

Ejemplo de la ejecución:

```[root@k8s-ma1 ~]# kubeadm init
[init] Using Kubernetes version: v1.13.1
[preflight] Running pre-flight checks
        [WARNING Hostname]: hostname "k8s-ma1" could not be reached
        [WARNING Hostname]: hostname "k8s-ma1": lookup k8s-ma1 on 192.168.160:53: no such host
[preflight] Pulling images required for setting up a Kubernetes cluster
[preflight] This might take a minute or two, depending on the speed of your internet connection
[preflight] You can also perform this action in beforehand using 'kubeadm config images pull'
[kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[kubelet-start] Activating the kubelet service
[certs] Using certificateDir folder "/etc/kubernetes/pki"
[certs] Generating "etcd/ca" certificate and key
[certs] Generating "etcd/healthcheck-client" certificate and key
[certs] Generating "etcd/server" certificate and key
[certs] etcd/server serving cert is signed for DNS names [k8s-ma1 localhost] and IPs [192.168.160.114 127.0.0.1 ::1]
[certs] Generating "etcd/peer" certificate and key
[certs] etcd/peer serving cert is signed for DNS names [k8s-ma1 localhost] and IPs [192.168.160.114 127.0.0.1 ::1]
[certs] Generating "apiserver-etcd-client" certificate and key
[certs] Generating "ca" certificate and key
[certs] Generating "apiserver" certificate and key
[certs] apiserver serving cert is signed for DNS names [k8s-ma1 kubernetes kubernetes.default kubernetes.default.svc kubernetes.default.svc.cluster.local] and IPs [10.96.0.1 192.168.160.114]
[certs] Generating "apiserver-kubelet-client" certificate and key
[certs] Generating "front-proxy-ca" certificate and key
[certs] Generating "front-proxy-client" certificate and key
[certs] Generating "sa" key and public key
[kubeconfig] Using kubeconfig folder "/etc/kubernetes"
[kubeconfig] Writing "admin.conf" kubeconfig file
[kubeconfig] Writing "kubelet.conf" kubeconfig file
[kubeconfig] Writing "controller-manager.conf" kubeconfig file
[kubeconfig] Writing "scheduler.conf" kubeconfig file
[control-plane] Using manifest folder "/etc/kubernetes/manifests"
[control-plane] Creating static Pod manifest for "kube-apiserver"
[control-plane] Creating static Pod manifest for "kube-controller-manager"
[control-plane] Creating static Pod manifest for "kube-scheduler"
[etcd] Creating static Pod manifest for local etcd in "/etc/kubernetes/manifests"
[wait-control-plane] Waiting for the kubelet to boot up the control plane as static Pods from directory "/etc/kubernetes/manifests". This can take up to 4m0s
[apiclient] All control plane components are healthy after 35.006355 seconds
[uploadconfig] storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
[kubelet] Creating a ConfigMap "kubelet-config-1.13" in namespace kube-system with the configuration for the kubelets in the cluster
[patchnode] Uploading the CRI Socket information "/var/run/dockershim.sock" to the Node API object "k8s-ma1" as an annotation
[mark-control-plane] Marking the node k8s-ma1 as control-plane by adding the label "node-role.kubernetes.io/master=''"
[mark-control-plane] Marking the node k8s-ma1 as control-plane by adding the taints [node-role.kubernetes.io/master:NoSchedule]
[bootstrap-token] Using token: a9oa2d.wuwa8o52xouxm6jl
[bootstrap-token] Configuring bootstrap tokens, cluster-info ConfigMap, RBAC Roles
[bootstraptoken] configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
[bootstraptoken] configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
[bootstraptoken] configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[bootstraptoken] creating the "cluster-info" ConfigMap in the "kube-public" namespace
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

Your Kubernetes master has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

You can now join any number of machines by running the following on each node
as root:

  kubeadm join 192.168.160.114:6443 --token a9oa2d.wuwa8o52xouxm6jl --discovery-token-ca-cert-hash sha256:432508f08d0a4628fddfb0e7d1448fa907d66e02c82eed691990f47d656ff9f3
```


Para continuar hay que adicionar los nodos worker, en cada nodo se ejecuta:
```
kubeadm join 192.168.160.114:6443 --token a9oa2d.wuwa8o52xouxm6jl --discovery-token-ca-cert-hash sha256:432508f08d0a4628fddfb0e7d1448fa907d66e02c82eed691990f47d656ff9f3
```

## Para acceder al Cluster a traves del loadbalancer
Para utilizar el loadbalancer en el cluster es necesario instalar el [MetalLB](https://metallb.universe.tf/) última versión como indica la [documentación oficial](https://metallb.universe.tf/installation/).

## Opcional I - Adicionar el dashborad de kubernetes 
Se puede instalar segun su web oficial [Web UI (Dashboard)](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/) con el comando:
``` 
kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml
```
Para crear un token de acceso al dashboard lo hacemos con la guia Crear un [usuario de ejemplo](https://github.com/kubernetes/dashboard/wiki/Creating-sample-user) creando un archivo ```dashboard-adminuser.yaml``` y usuando el comando ```kubectl apply -f dashboard-adminuser.yaml```
```
# Contenido del archivo dashboard-adminuser.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kube-system
  
---

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kube-system

```
se accede al dashboard a través del servicio kubernetes-dashboard con el token de la cuenta admin-user que se obtiene
```
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}') 
```

## Opcional II Instalacion de GitLab CE

Para instalar el Gitlab CE en el cluster siguimos la documentación oficial de GitLab en [GitLan Helm Chart](https://docs.gitlab.com/ee/install/kubernetes/gitlab_chart.html) siguiendo 3 pasos fundamentales:
1. Instalar el Helm
2. Dar permiso al demonio Tiller
3. Instalar el Gitlab CE 

### 1. Instalar el Helm
Instalar el Helm por esta [documentación oficial](https://docs.helm.sh/using_helm/#installing-helm).
### 2. Dar permiso al demonio Tiller
Damos permiso al demonio Tiller con la [documentación oficial](https://docs.gitlab.com/ee/install/kubernetes/preparation/tiller.html)
### 3. Instalar el Gitlab CE 
Instalar el Gitlab CE utilizando el comando para la versión Community Edition nosotros recomendamos utilizar un namespace como ns-gitlab, pero no es obligatorio. Tambien se habilita el runner con la opcion privileged para poder hacer imagenes docker
```
helm upgrade --install gitlab gitlab/gitlab \
  --timeout 600 --namespace ns-gitlab \
  --set global.hosts.domain=dev.migapp.site  \
  --set global.hosts.externalIP=192.168.160.246 \
  --set certmanager-issuer.email=miguelciego@gmail.com \
--set gitlab.migrations.image.repository=registry.gitlab.com/gitlab-org/build/cng/gitlab-rails-ce \
--set gitlab.sidekiq.image.repository=registry.gitlab.com/gitlab-org/build/cng/gitlab-sidekiq-ce  \
--set gitlab.unicorn.image.repository=registry.gitlab.com/gitlab-org/build/cng/gitlab-unicorn-ce \
--set gitlab.unicorn.workhorse.image=registry.gitlab.com/gitlab-org/build/cng/gitlab-workhorse-ce \
--set gitlab.task-runner.image.repository=registry.gitlab.com/gitlab-org/build/cng/gitlab-task-runner-ce \
--set gitlab-runner.runners.image=ubuntu:16.04   --set gitlab-runner.runners.privileged=true
```




